import sys
# from taurus.qt.qtgui.application import TaurusApplication
# from pyqtgraph.Qt import QtGui, QtCore
# import pyqtgraph as pg


# import numpy as np
import os
from PyQt5 import Qt, uic


# python -m pyqtgraph.examples


# TODO: check format files.

class Conversor(Qt.QWidget):

    def __init__(self, parent=None):
        # call the parent class init
        Qt.QWidget.__init__(self, parent=parent)
        # load the .ui file
        uic.loadUi(os.path.join(os.path.dirname(__file__), "converter.ui"), self)
        self.bChoose.clicked.connect(self.openFile())
        self.bConv.clicked.connect(self.convert(self.textBox.getText))

    def openFile(self):
        """to check"""
        fileName, _ = Qt.QFileDialog.getOpenFileName(self, 'txt rga files', Qt.QDir.rootPath(), '*.txt')
        self.lineFile.setText(fileName)

    def convert(self, file):
        # f = input("rgaToConvert.txt")
        file = open(file, "r")
        textMKS = file.read()
        # = textMKS.replace('"', '')
        file.close()

        lines = textMKS.split("\n")
        # print("largo archivo", len(lines))
        massLine = lines[56]

        """take masses list in float format"""
        massLine = massLine.replace('"Mass ', "")
        massLine = massLine.replace('"', '')
        massLine = massLine.split()
        del massLine[0:2]
        del massLine[-3:]
        massFlt = [float(f) for f in massLine]
        """"DONE"""

        """  DONE """
        for i in range((len(lines)) - 58):
            ppLine = lines[i + 57]
            ppLine = ppLine.replace('"', '')
            ppLine = ppLine.split()
            """date"""
            date = ppLine[0:3]
            print(date)
            if (date[2] == 'PM'):
                hora = date[1].split(":")
                hora[0] = int(hora[0]) + 12
                hora = (str(hora[0]) + ":" + hora[1] + ":" + hora[2])
                print("HORA", hora)
            date[0] = date[0].replace("/", "-")
            print(date)
            ppressures = ppLine[4:-1]
            ppFlt = [float(f) for f in ppressures]
            strHeader = (lines[8], lines[9], date)

            strName = str(lines[8])

            strName = strName.split("\t")
            strName = str(strName[1])
            strName = strName[1:-1]
            strName = (strName + "_" + date[0] + "_" + date[1] + ".txt")
            # print(massFlt)
            print(massLine)
            print(strHeader)
            print(strName)
            try:
                a = open(strName, "w")
                a.write(str(strHeader) + "\n")
                a.write(str(massFlt) + "\n")
                a.write(str(ppressures) + "\n")
                a.close()
                print(strName, " Successfully created")
            except:
                print("mal")


if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = Conversor()
    w.setWindowTitle("RGA file Conversor")

    # show it (if you do not show the widget, it won't be visible)
    w.show()

    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())
